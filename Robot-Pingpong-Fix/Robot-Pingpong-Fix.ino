/*******************************************
  Project Akhir Robot pelontar Bola Pingpong
*******************************************/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <Servo.h>

Servo myservo1, myservo2, myServo3, mbldc1, mbldc2, mbldc3;  // create servo object to control a servo

// control the servo is attached to
static const int servoPin1 = D1;
static const int servoPin2 = D2;
static const int servoPin3 = D3;
static const int servoPin4 = D6;
static const int servoPin5 = D7;
byte servoPin = D5;

// Replace with your network credentials
const char* ssid     = "Asus";
const char* password = "kurni17asdfghjkl";

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Decode HTTP GET value
String valueString1, valueString2, valueString3, valueString4, valueString5 = String(10);
int pos1 = 0;
int pos2 = 0;

// Current time
unsigned long currentTime = millis();
// Previous time
unsigned long previousTime = 0;
// Define timeout time in milliseconds (example: 2000ms = 2s)
const long timeoutTime = 1000;


// Set your Static IP address
IPAddress local_IP(192, 168, 43, 17);
// Set your Gateway IP address
IPAddress gateway(192, 168, 43, 1);

IPAddress subnet(255, 255, 0, 0);
IPAddress primaryDNS(8, 8, 8, 8);   //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional

void setup() {
  Serial.begin(115200);

  myservo1.attach(servoPin1);  // attaches the servo on the servoPin to the servo object
  myservo1.write(90);          // start position in 90°
  myservo2.attach(servoPin2);  // attaches the servo on the servoPin to the servo object
  myservo2.write(90);          // start position in 90°
  mbldc1.attach(servoPin3);
  mbldc1.write(1000);
  mbldc2.attach(servoPin4);
  mbldc2.write(1000);
  mbldc3.attach(servoPin5);
  mbldc3.write(1000);

  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA Failed to configure");
  }

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    currentTime = millis();
    previousTime = currentTime;
    Serial.println("Control Action Active");       // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected() && currentTime - previousTime <= timeoutTime) { // loop while the client's connected
      currentTime = millis();
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
            // if the current line is blank, you got two newline characters in a row.
            // that's the end of the client HTTP request, so send a response:

          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            if (header.indexOf("GET /Mode=1") != -1)  {
              myservo1.write(75); //Moving servo to 75 degree
              myservo2.write(70); //Moving servo to 70 degree
              myServo3.write(190);
              mbldc1.write(1195);
              mbldc2.write(1265);
              mbldc3.write(1235);
            }
            else if (header.indexOf("GET /Mode=2") != -1)  {
              myservo1.write(75); //Moving servo to 75 degree
              myservo2.write(90); //Moving servo to 90 degree
              myServo3.write(190);
              mbldc1.write(1195);
              mbldc2.write(1265);
              mbldc3.write(1235);
            }
            else if (header.indexOf("GET /Mode=3") != -1)  {
              myservo1.write(75); //Moving servo to 75 degree
              myservo2.write(110); //Moving servo to 110 degree
              myServo3.write(190);
              mbldc1.write(1195);
              mbldc2.write(1265);
              mbldc3.write(1235);
            }
            else if (header.indexOf("GET /Mode=4") != -1)  {
              myservo1.write(85); //Moving servo to 85 degree
              myservo2.write(70); //Moving servo to 70 degree
              myServo3.write(190);
              mbldc1.write(1195);
              mbldc2.write(1270);
              mbldc3.write(1240);
            }
            else if (header.indexOf("GET /Mode=5") != -1)  {
              myservo1.write(85); //Moving servo to 85 degree
              myservo2.write(90); //Moving servo to 90 degree
              myServo3.write(190);
              mbldc1.write(1195);
              mbldc2.write(1270);
              mbldc3.write(1240);
            }
            else if (header.indexOf("GET /Mode=6") != -1)  {
              myservo1.write(85); //Moving servo to 85 degree
              myservo2.write(110); //Moving servo to 110 degree
              myServo3.write(190);
              mbldc1.write(1195);
              mbldc2.write(1270);
              mbldc3.write(1240);
            }

            // Header HTML web page
            client.println("<!DOCTYPE html>");
            client.println("<html lang=\"en\"><head><meta charset=\"utf-8\">");
            client.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">");
            client.println("<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css\" integrity=\"sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2\" crossorigin=\"anonymous\">");
            client.println("<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css\">");
            client.println("<link rel=\"stylesheet\" href=\"https://pro.fontawesome.com/releases/v5.10.0/css/all.css\" integrity=\"sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p\" crossorigin=\"anonymous\" />");
            client.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>");
            client.println("<title>Robot Pingpong</title>");

            // css style Web Page
            client.println("<style> :root { --main-color: #555555; }");
            client.println("* { font-family: 'Poppins', sans-serif; margin: 0; padding: 0; text-decoration: none; list-style-type: none; box-sizing: border-box; }");
            client.println("#sidebar-toggle { display: none; }");
            client.println("#side { padding-left: 1rem; font-weight: bold; font-size: 0.85rem; color: #fff; }");
            client.println("#logo { padding-right: 0.4rem; } #carouselImage img { height: 596px; }");
            client.println("#optionMode { padding: 0rem 4rem; } #Mode-Control { padding: 1rem; }");
            client.println("#Project { padding: .5rem; width: 25%; display: inline-flex; }");
            client.println("#menu { height: 86%; } .btn { color: #212529; width: 8rem; }");
            client.println(".btn-primary { background-color: transparent; } .btn-success { background-color: transparent; } .btn-warning { background-color: transparent; }");
            client.println(".carousel-caption { bottom: 200px; }");
            client.println(".carousel-caption h5 { font-size: 60px; text-transform: uppercase; margin-top: 25px; font-weight: 900; font-family: inherit; }");
            client.println(".sidebar { height: 100%; width: 240px; position: fixed; left: 0; top: 0; z-index: 100; background: var(--main-color); color: #fff; overflow-y: auto; transition: 500ms; }");
            client.println(".sidebar-header { display: flex; justify-content: space-between; align-items: center; height: 60px; padding: 0rem 1.2rem; }");
            client.println(".brand { font-size: 1.2rem; } .sidebar-menu { padding: 1.5rem; } .sidebar li { margin-bottom: 1.5rem; }");
            client.println(".sidebar a { color: #fff; cursor: pointer; } .sidebar a:hover { color: #FFD523 !important; cursor: pointer; text-decoration: none; }");
            client.println(".sidebar a span:hover { color: #FFD523 !important; cursor: pointer; }");
            client.println("#sidebar-toggle:checked~.sidebar { width: 60px; }");
            client.println("#sidebar-toggle:checked~.sidebar .sidebar-header h3 span, #sidebar-toggle:checked~.sidebar li span { display: none; }");
            client.println("#sidebar-toggle:checked~.sidebar .sidebar-header, #sidebar-toggle:checked~.sidebar li { display: flex; justify-content: center; }");
            client.println("#sidebar-toggle:checked~.main-content { margin-left: 60px; }");
            client.println("#sidebar-toggle:checked~.main-content header { left: 60px; width: calc(100% - 60px); }");
            client.println(".main-content { position: relative; margin-left: 240px; transition: 500ms; }");
            client.println("header { position: fixed; left: 240px; top: 0; z-index: 100; width: calc(100% - 240px); background: #D2D3C9; height: 60px; padding: 0rem 1rem; display: flex; align-items: center; justify-content: space-between; border-bottom: 2px solid #ccc; transition: 500ms;}");
            client.println(".user-wrapper { display: flex; align-items: center; }");
            client.println(".user-wrapper h4 { display: flex; align-items: center; padding-top: 1rem; line-height: 0.4rem; font-size: 17px; }");
            client.println(".user-wrapper img { border-radius: 50%; margin-right: 1rem; }");
            client.println(".user-wrapper small { display: inline-block; color: #305F72; } body { background-color: #EBE6E6; }");
            client.println("main { margin-top: 60px; background: #EBE6E6; min-height: 90vh; } .dash-title { padding: 1rem 1.5rem 0rem 1.5rem }");
            client.println(".dash-cards { display: grid; grid-template-columns: 48% 48%; grid-column-gap: 1.1rem; padding-left: 2.2% }");
            client.println(".card-single { background: #fff; border-radius: 7px; box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.2); margin-top: 3%; }");
            client.println(".card-body { padding: 0.5rem 1rem; display: flex; align-items: center; }");
            client.println(".card-body span { font-size: 1.5rem; color: #065C6F; padding-right: 1.4rem; }");
            client.println(".card-body h5 { color: #065C6F; font-size: 1rem; margin-top: 10px; } .card-mode { padding: 1rem 1rem; }");
            client.println(".card-footer { text-align: center; font-size: 15px; padding: .1rem 1.25rem; }");
            client.println(".card-footer h4 { text-align: justify; padding: 1rem 0rem 0rem 1rem; font-size: 1rem; }");
            client.println(".card-footer p { margin-bottom: 2px; font-size: 14px; } .card-about { padding: 1.5rem; }");
            client.println(".slider { width: 80%; } .recent { margin-top: 3rem; margin-bottom: 3rem; }");
            client.println(".activity-grid { display: grid; grid-template-columns: 60% 40%; grid-column-gap: 1.5rem; padding: 1rem; }");
            client.println(".activity-card, .summary-card, .bday-card { background: #fff; border-radius: 7px; box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.2); }");
            client.println(".activity-card h3 { margin: 1rem; } .summary-card { margin-bottom: 1.5rem; padding-top: .5rem; padding-bottom: .5rem; }");
            client.println(".bday-card { padding: 1rem; }");
            client.println("@media only screen and (max-width: 1200px) { .sidebar { width: 60px; z-index: 150; }");
            client.println(".sidebar .sidebar-header h3 span, .sidebar li span { display: none; }");
            client.println(".sidebar .sidebar-header, .sidebar li { display: flex; justify-content: center; } .main-content { margin-left: 60px; }");
            client.println(".main-content header { left: 60px; width: calc(100% - 60px); } #sidebar-toggle:checked~.sidebar { width: 57%; }");
            client.println("#sidebar-toggle:checked~.sidebar .sidebar-header h3 span, #sidebar-toggle:checked~.sidebar li span { display: inline; }");
            client.println("#sidebar-toggle:checked~.sidebar .sidebar-header { display: flex; justify-content: space-between; } #sidebar-toggle:checked~.sidebar li { display: block; }");
            client.println("#sidebar-toggle:checked~.main-content { margin-left: 60px; } #sidebar-toggle:checked~.main-content header { left: 60px; }");
            client.println("#optionMode { padding: 2px; display: inline-flex; } }");
            client.println("@media only screen and (max-width: 860px) { .dash-cards { grid-template-columns: repeat(2, 1fr); padding: 0% 8% 0% 5%; }");
            client.println(".card-single { margin-bottom: 1rem; } .activity-grid { display: block; } .summary { margin-top: 1.5rem; }");
            client.println(".carousel-caption h5 { font-size: 40px; text-transform: uppercase; margin-top: 25px; font-family: inherit; }");
            client.println("#optionMode { padding: 2px; display: inline-flex; } #Project { width: 40%; display: inline-flex; } }");
            client.println("@media only screen and (max-width: 600px) {.dash-cards { grid-template-columns: 100%; padding: 0% 8% 0% 5%; } }");
            client.println("@media only screen and (max-width: 450px) { main { padding-left: 0rem; padding-right: 0rem; }");
            client.println("#carouselImage img { height: 370px; } .carousel-caption { bottom: 80px; }");
            client.println(".carousel-caption h5 { font-size: 24px; text-transform: uppercase; margin-top: 25px; font-family: monospace; }");
            client.println(".carousel-caption p { font-size: 11px; font-family: inherit; } .btn { padding: 4px; width: auto; }");
            client.println(".card-mode { padding: 10px 5px; } #Mode-Control { padding: 0px; } #optionMode { padding: 2px; display: inline-flex; }");
            client.println("#Project { width: 90%; display: inline-flex; } #menu { height: 80%; } }");
            client.println("</style></head>");

            // Body Web Page
            client.println("<body><input type=\"checkbox\" id=\"sidebar-toggle\"><div class=\"sidebar\">");
            client.println("<div class=\"sidebar-header\"><h3 class=\"brand\">");
            client.println("<span style=\"font-family:monospace;\"><i class=\"fad fa-table-tennis\" id=\"logo\"></i> Robopong </span>");
            client.println("</h3><label for=\"sidebar-toggle\" class=\"ti-menu-alt\"></label></div><div class=\"sidebar-menu\">");
            client.println("<ul><li data-li=\"Home\" class=\"active\"><a class=\"ti-home\"><span id=\"side\">Home</span></a></li>");
            client.println("<li data-li=\"Manual-Control\"><a class=\"ti-agenda\"><span id=\"side\">Manual Control</span></a></li>");
            client.println("<li data-li=\"Mode-Control\"><a class=\"ti-book\"><span id=\"side\">Mode Control</span></a></li>");
            client.println("<li data-li=\"About\"><a class=\"ti-settings\"><span id=\"side\">About</span></a></li></ul></div></div>");
            client.println("<div class=\"main-content\"><header><div class=\"header\"></div><div class=\"user-wrapper\">");
            client.println("<img src=\"https://infoberuang.files.wordpress.com/2012/05/batandball.png\" width=\"40px\" height=\"40px\" alt=\"\">");
            client.println("<div><h4>Robot Pingpong</h4><small>Home Made</small></div></div></header>");

            // Menu Home Web Page
            client.println("<main><div class=\"item Home\" id=\"home\"><div id=\"carouselImage\" class=\"carousel slide carousel-fade\" data-ride=\"carousel\">");
            client.println("<ol class=\"carousel-indicators\"><li data-target=\"#carouselImage\" data-slide-to=\"0\" class=\"active\"></li>");
            client.println("<li data-target=\"#carouselImage\" data-slide-to=\"1\"></li><li data-target=\"#carouselImage\" data-slide-to=\"2\"></li></ol>");
            client.println("<div class=\"carousel-inner\"><div class=\"carousel-item active\">");
            client.println("<img src=\"https://www.publicdomainpictures.net/pictures/230000/velka/ping-pong-sport.jpg\" class=\"d-block w-100\">");
            client.println("<div class=\"carousel-caption\"><h5>Welcome <br>Robot Pingpong</h5>");
            client.println("<p>Robot Pelontar Bola Pingpong Buatan Rumahan Dengan Sistem Remote Control Web Based Application Berbasis Internet of Thigns (IoT)</p>");
            client.println("</div></div><div class=\"carousel-item\">");
            client.println("<img src=\"https://thefederal.com/file/2019/07/c700x420.jpg\" class=\"d-block w-100\">");
            client.println("<div class=\"carousel-caption\"><h5>Welcome <br>Robot Pingpong</h5>");
            client.println("<p>Robot Pelontar Bola Pingpong Buatan Rumahan Dengan Sistem Remote Control Web Based Application Berbasis Internet of Thigns (IoT)</p>");
            client.println("</div></div><div class=\"carousel-item\">");
            client.println("<img src=\"https://spaces-wp.imgix.net/2016/01/Top-The-Tables-II_Web-780x400.png?auto=compress,format&q=50\" class=\"d-block w-100\">");
            client.println("<div class=\"carousel-caption\"><h5>Welcome <br>Robot Pingpong</h5>");
            client.println("<p>Robot Pelontar Bola Pingpong Buatan Rumahan Dengan Sistem Remote Control Web Based Application Berbasis Internet of Thigns (IoT)</p>");
            client.println("</div></div></div><a class=\"carousel-control-prev\" href=\"#carouselImage\" role=\"button\" data-slide=\"prev\">");
            client.println("<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span><span class=\"sr-only\">Previous</span></a>");
            client.println("<a class=\"carousel-control-next\" href=\"#carouselImage\" role=\"button\" data-slide=\"next\">");
            client.println("<span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span><span class=\"sr-only\">Next</span></a></div></div>");

            // Menu Manual-Control Web Page
            client.println("<div class=\"item Manual-Control\" style=\"display: none;\"><h2 class=\"dash-title\">Manual Control</h2>");
            client.println("<div class=\"dash-cards\"><div class=\"card-single\"><div class=\"card-body\">");
            client.println("<span><i class=\"fad fa-sync-alt\"></i></span><div><h5>Servo Control</h5></div></div>");
            client.println("<div class=\"card-footer\" id=\"menu\"><h4>Servo Top</h4><p>Position: <span id=\"servoPos1\"></span>°</p>");
            client.println("<input type=\"range\" min=\"30\" max=\"130\" value=\"80\" class=\"slider\" id=\"servoSlider1\" onchange=\"servo1(this.value)\" value1=\""+valueString1+"\"/>");
            client.println("<h4>Servo Bottom</h4><p>Position: <span id=\"servoPos2\"></span>°</p>");
            client.println("<input type=\"range\" min=\"60\" max=\"120\" value=\"90\" class=\"slider\" id=\"servoSlider2\" onchange=\"servo2(this.value)\" value2=\""+valueString2+"\"/>");
            client.println("<br><br></div></div>");
            client.println("<div class=\"card-single\"><div class=\"card-body\"><span><i class=\"fad fa-recycle\"></i></span>");
            client.println("<div><h5>Brushless Signal Control</h5></div></div><div class=\"card-footer\">");
            client.println("<h4>Brushless Motor 1</h4><p>Speed: <span id=\"Brushless1\"></span> (µs)</p>");
            client.println("<input type=\"range\" min=\"1000\" max=\"2000\" value=\"1200\" class=\"slider\" id=\"BrushlessSlider1\" onchange=\"bldc1(this.value)\" value3=\""+valueString3+"\"/>");
            client.println("<h4>Brushless Motor 2</h4><p>Speed: <span id=\"Brushless2\"></span> (µs)</p>");
            client.println("<input type=\"range\" min=\"1000\" max=\"2000\" value=\"1260\" class=\"slider\" id=\"BrushlessSlider2\" onchange=\"bldc2(this.value)\" value4=\""+valueString4+"\"/>");
            client.println("<h4>Brushless Motor 3</h4><p>Speed: <span id=\"Brushless3\"></span> (µs)</p>");
            client.println("<input type=\"range\" min=\"1000\" max=\"2000\" value=\"1230\" class=\"slider\" id=\"BrushlessSlider3\" onchange=\"bldc3(this.value)\" value5=\""+valueString5+"\"/>");
            client.println("<br><br></div></div></div></div>");

            // Menu Mode-Control Web Page
            client.println("<div class=\"item Mode-Control\" id=\"Mode-Control\" style=\"display: none;\"><div class=\"activity-grid\">");
            client.println("<div class=\"activity-card\"><div class=\"card-body\"><span><i class=\"fas fa-server\"></i></span><div>");
            client.println("<h5>Mode Control</h5></div></div><div class=\"card-footer\"><div class=\"card-all-mode\" style=\"padding: 1rem 0rem;\">");
            client.println("<div class=\"row\" id=\"optionMode\"><div class=\"card-mode\"><a href=\"/Mode=1\" class=\"btn btn-primary\">MODE 1</a></div>");
            client.println("<div class=\"card-mode\"><a href=\"/Mode=2\" class=\"btn btn-primary\">MODE 2</a></div>");
            client.println("<div class=\"card-mode\"><a href=\"/Mode=3\" class=\"btn btn-primary\">MODE 3</a></div></div>");
            client.println("<div class=\"row\" id=\"optionMode\"><div class=\"card-mode\"><a href=\"/Mode=4\" class=\"btn btn-success\">MODE 4</a></div>");
            client.println("<div class=\"card-mode\"><a href=\"/Mode=5\" class=\"btn btn-success\">MODE 5</a></div>");
            client.println("<div class=\"card-mode\"><a href=\"/Mode=6\" class=\"btn btn-success\">MODE 6</a></div></div>");
            client.println("</div></div></div></div></div>");

            // Menu About Web Page
            client.println("<div class=\"item About\" style=\"display: none;\"><div class=\"card text-center\">");
            client.println("<div class=\"card-header\">\"ROBOPONG\"</div>");
            client.println("<div class=\"card-about\"><h5 class=\"card-title\">Robot Pelontar Bola Pingpong</h5>");
            client.println("<p class=\"card-text\">Ping-Pong Ball Throwing Robot With Control System Based On Internet Of Things (Iot) With Home Made & Home Products.</p>");
            client.println("<a href=\"https://gitlab.com/ryankurni17/robot-pingpong\" class=\"btn btn-outline-secondary\" id=\"Project\">Project-Robot-Pingpong ~GitLab~</a>");
            client.println("</div><div class=\"card-footer text-muted\">#Copyright Robot-Pingpong 2021");
            client.println("</div></div></div></main></div>");

            // java script Web Page
            client.println("<script>var li_elements = document.querySelectorAll(\".sidebar-menu ul li\");");
            client.println("var item_elements = document.querySelectorAll(\".item\");");
            client.println("for (var i = 0; i < li_elements.length; i++) {");
            client.println("li_elements[i].addEventListener(\"click\", function() {");
            client.println("li_elements.forEach(function(li) {");
            client.println("li.classList.remove(\"active\");});");
            client.println("this.classList.add(\"active\");");
            client.println("var li_value = this.getAttribute(\"data-li\");");
            client.println("item_elements.forEach(function(item) {");
            client.println("item.style.display = \"none\";});");
            client.println("if (li_value == \"Home\") {");
            client.println("document.querySelector(\".\" + li_value).style.display = \"block\";");
            client.println("} else if (li_value == \"Manual-Control\") {");
            client.println("document.querySelector(\".\" + li_value).style.display = \"block\";");
            client.println("} else if (li_value == \"Mode-Control\") {");
            client.println("document.querySelector(\".\" + li_value).style.display = \"block\";");
            client.println("} else if (li_value == \"About\") {");
            client.println("document.querySelector(\".\" + li_value).style.display = \"block\";");
            client.println("} else { console.log(\"\"); } }); }");

            client.println("var slider1 = document.getElementById(\"servoSlider1\");");
            client.println("var servoP1 = document.getElementById(\"servoPos1\");");
            client.println("servoP1.innerHTML = slider1.value;slider1.oninput = function() {");
            client.println("slider1.value = this.value;servoP1.innerHTML = this.value;}");
            client.println("$.ajaxSetup({timeout: 1000});function servo1(pos1) {");
            client.println("$.get(\"/?value1=\" + pos1 + \"&\"); {Connection: close};}");

            client.println("var slider2 = document.getElementById(\"servoSlider2\");");
            client.println("var servoP2 = document.getElementById(\"servoPos2\");");
            client.println("servoP2.innerHTML = slider2.value;slider2.oninput = function() {");
            client.println("slider2.value = this.value;servoP2.innerHTML = this.value;}");
            client.println("$.ajaxSetup({timeout: 1000});function servo2(pos2) {");
            client.println("$.get(\"/?value2=\" + pos2 + \"&\"); {Connection: close};}");

            client.println("var slider3 = document.getElementById(\"BrushlessSlider1\");");
            client.println("var Mblcd1 = document.getElementById(\"Brushless1\");");
            client.println("Mblcd1.innerHTML = slider3.value;slider3.oninput = function() {");
            client.println("slider3.value = this.value;Mblcd1.innerHTML = this.value;}");
            client.println("$.ajaxSetup({timeout: 1000});function bldc1(pos3) {");
            client.println("$.get(\"/?value3=\" + pos3 + \"&\"); {Connection: close};}");

            client.println("var slider4 = document.getElementById(\"BrushlessSlider2\");");
            client.println("var Mblcd2 = document.getElementById(\"Brushless2\");");
            client.println("Mblcd2.innerHTML = slider4.value;slider4.oninput = function() {");
            client.println("slider4.value = this.value;Mblcd2.innerHTML = this.value;}");
            client.println("$.ajaxSetup({timeout: 1000});function bldc2(pos4) {");
            client.println("$.get(\"/?value4=\" + pos4 + \"&\"); {Connection: close};}");

            client.println("var slider5 = document.getElementById(\"BrushlessSlider3\");");
            client.println("var Mblcd3 = document.getElementById(\"Brushless3\");");
            client.println("Mblcd3.innerHTML = slider5.value;slider5.oninput = function() {");
            client.println("slider5.value = this.value;Mblcd3.innerHTML = this.value;}");
            client.println("$.ajaxSetup({timeout: 1000});function bldc3(pos5) {");
            client.println("$.get(\"/?value5=\" + pos5 + \"&\"); {Connection: close};}</script>");

            client.println("</body></html>");


            //GET /?value=180& HTTP/1.1
            if(header.indexOf("GET /?value1=")>=0) {
              pos1 = header.indexOf('=');
              pos2 = header.indexOf('&');
              valueString1 = header.substring(pos1+1, pos2);

              //Rotate the servo
              myservo1.write(valueString1.toInt());
              Serial.print("Servo Top : ");
              Serial.print(valueString1);
              Serial.println("°\n");
            }

            if(header.indexOf("GET /?value2=")>=0) {
              pos1 = header.indexOf('=');
              pos2 = header.indexOf('&');
              valueString2 = header.substring(pos1+1, pos2);

              //Rotate the servo
              myservo2.write(valueString2.toInt());
              Serial.print("Servo Bottom : ");
              Serial.print(valueString2);
              Serial.println("°\n");
            }

            if(header.indexOf("GET /?value3=")>=0) {
              pos1 = header.indexOf('=');
              pos2 = header.indexOf('&');
              valueString3 = header.substring(pos1+1, pos2);

              //Rotate the servo
              mbldc1.write(valueString3.toInt());
              Serial.print("Brushless Motor 1 : ");
              Serial.print(valueString3);
              Serial.println(" µs \n");
              myServo3.attach(servoPin);
              myServo3.write(190);

              if (valueString3 == "1178"){
                    myServo3.attach(servoPin);
                    myServo3.write(1500); // Set Kalibrasi posisi 0
                  }
            }

            if(header.indexOf("GET /?value4=")>=0) {
              pos1 = header.indexOf('=');
              pos2 = header.indexOf('&');
              valueString4 = header.substring(pos1+1, pos2);

              //Rotate the servo
              mbldc2.write(valueString4.toInt());
              Serial.print("Brushless Motor 2 : ");
              Serial.print(valueString4);
              Serial.println(" µs \n");
            }

            if(header.indexOf("GET /?value5=")>=0) {
              pos1 = header.indexOf('=');
              pos2 = header.indexOf('&');
              valueString5 = header.substring(pos1+1, pos2);

              //Rotate the servo
              mbldc3.write(valueString5.toInt());
              Serial.print("Brushless Motor 3 : ");
              Serial.print(valueString5);
              Serial.println(" µs \n");
            }

            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the control connection
    client.stop();
    Serial.println("### control has been Success ###");
    Serial.println("");
  }
}
